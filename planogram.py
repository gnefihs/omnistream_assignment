def shelve_products(C,R,Ws):
    """
    Args
    C  : np.array of shelf capacity(cm) of c shelves, dims = (c,)
    R  : np.array of the profit/width(mm) ratios of N products, dims = (N,)
    Ws : np.array of product width, sorted ascendingly by R, dims = (N,)
    
    Output
    results    : a list of lists
    results[k] : a list of indices of products (after sorting by R) on shelf k
    """
    # initialize product availability, starting with all available
    avail = np.ones(len(R), dtype=int)
    results = [[] for x in range(len(C))]

    # start from the last index i.e. highest profit/width ratio
    i = len(R)-1

    # do it shelf by shelf
    for s,shelf_capacity in enumerate(C):
        # counter for products skipped when filling one shelf
        skipped = 0

        # find the min Width of available items
        Wmin = min(Ws[avail==1])

        # While there's still space for the smallest item available...
        while (shelf_capacity > Wmin) & (i > 0):
            # check product is available
            if avail[i] == 1:
                # check product fits on current shelf
                if (shelf_capacity - Ws[i] >= 0):
                    shelf_capacity-=Ws[i]
                    avail[i] = 0
                    results[s].append(i)
#                     print('placed product {}:{}mm onto shelf {}. shelf_cap {}'\
#                     .format(i,W[i],s,shelf_capacity))
                    
                else:
                    # if product doesn't fit, add 1 to products skipped
                    skipped+=1    
            else:
                 # if product is not available, add 1 to product skipped
                skipped+=1

            i-=1

        # backtrack by the amount skipped when filling one shelf
        i+=skipped+1
    return results


def planogram(fixture, products):
    """
    Arguments:
    - fixture :: DataFrame[["shelf_no", "shelf_width_cm"]]
    - products :: DataFrame[["product_id", "product_width_mm", "profit"]]

    Returns: DataFrame[["shelf_no", "product_id"]]
    """
    
    #capacity in  mm
    C = fixture['shelf_width_cm'].values*10
    #width
    W = products['product_width_mm'].values
    #profit
    P = products['profit'].values
    #profit/width ratio
    R = P/W

    # sort W by products' profit/width ratios, R (ascending)
    order = np.argsort(R)
    Ws = W[order]
    
    # get results in list form
    results = shelve_products(C,R,Ws)
    
    # create ['shelf_no','product_id'] dataframe
    product_shelf = pd.DataFrame(products['product_id'])
    product_shelf['shelf_no'] = 0
    # reorder cols
    product_shelf = product_shelf[['shelf_no','product_id']]
    
    # assign products to the correct shelf
    for i, shelf_result in enumerate(results):
        rows = order[shelf_result]
    #     products.iloc[rows]['shelf_no'] = i
        product_shelf.loc[rows, 'shelf_no'] = i+1
    
    
    
    return product_shelf


if __name__ == "__main__":
    import numpy as np
    import pandas as pd
    
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--products",
        type=argparse.FileType(mode="r"),
        default="products.csv",
        help="products input file",
    )
    parser.add_argument(
        "--fixture",
        type=argparse.FileType(mode="r"),
        default="fixture.csv",
        help="fixture input file",
    )
    parser.add_argument(
        "--out", "-o", default="solution.csv", help="solution output file"
    )

    args = parser.parse_args()
    
    fixture = pd.read_csv(args.fixture)
    products = pd.read_csv(args.products)

    solution = planogram(fixture, products)
    
    solution.to_csv(args.out, index=False)

    print(
        "stats:",
        solution[["shelf_no", "product_id"]]
        .merge(fixture, on="shelf_no")
        .merge(products, on="product_id")
        .assign(n_products=1)
        .pivot_table(
            index=["shelf_no", "shelf_width_cm"],
            values=["n_products", "profit", "product_width_mm"],
            aggfunc=np.sum,
            margins=True,
        ),
        sep="\n",
    )