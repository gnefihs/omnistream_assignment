## Shifeng's submission, 19 Mar 2020
#### Approach
* A greedy approach using profit-to-width ratio is implemented.
 * the solution might not be optimal
 
#### Method
* Products are assigned to shelves based on their profit-to-width ratio, R.
 * Products with the highest R are placed first.
* Assignment is done shelf by shelf in an arbitrary order.
* For a particular shelf, when the product with the highest R (amongst products not yet assigned) cannot fit on the shelf, the product with the highest R that fits is assigned.

#### Analysis
* 13mm of shelf space is unused, compared to 3254mm of total shelf space, and the smallest item with width 19mm 
 * hence the result should be closed to optimal
* Algorithm runs in time O(nlogn) approx.

---
## Task
The task is to place **products** into the **fixture**, maximizing product profit subject to the usual knapsack problem constraints:

-   **0/1 constraint:** there must be either zero or one of each product on the shelves
-   **shelf capacity constraint:** the sum of the product widths must not exceed the shelf width

`planogram.py` contains a minimal implementation.

To proceed:

1.  clone this repository
2.  **working on a new branch**, complete the implementation of `planogram.py:planogram()`.
    You are free to use your choice of additional libraries;
    make sure to update `pyproject.toml` / `requirements.txt` as necessary.
3.  get the code back to us:
    -   **private** repository on bitbucket (preferred) github, etc, or
    -   zip/tar project files and email us
